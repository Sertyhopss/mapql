package com.example.model.mapper;

import com.example.model.nosql.NoSqlFlight;
import com.example.model.nosql.NoSqlHeartbeat;
import com.example.model.sql.Drone;
import com.example.model.sql.Flight;
import com.example.model.sql.Heartbeat;
import com.example.model.sql.Operator;
import com.example.service.MapqlService;
import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class NosqlMapper {

    public NoSqlFlight toNoSqlFlight(Flight flight) {
        Operator operator = MapqlService.getOperators().get(flight.getOperatorId());
        Drone drone = MapqlService.getDrones().get(flight.getDroneUuid());
        return NoSqlFlight.builder()
                .flightUuid(flight.getUuid())
                .droneUuid(drone.getUuid())
                .droneSerial(drone.getSerial())
                .imei(drone.getImei())
                .operatorId(operator.getId())
                .operatorLogin(operator.getLogin())
                .operatorName(operator.getName())
                .operatorCompany(operator.getCompany())
                .launches(flight.getLaunches())
                .area(flight.getArea())
                .routeLen(flight.getRouteLen())
                .surveyObject(MapqlService.getSurveyObjects().get(flight.getSurveyObjectId()).getName())
                .track(getNoSqlHeartbeats(flight.getStartHeartbeatId(), flight.getEndHeartbeatId()))
                .build();
    }

    public NoSqlHeartbeat toNoSqlHeartbeat(Heartbeat heartbeat) {
        return NoSqlHeartbeat.builder()
                .flightUuid(heartbeat.getFlightUuid())
                .source(heartbeat.getSource())
                .autopilotState(heartbeat.getAutopilotState())
                .batteryVoltage(heartbeat.getBatteryVoltage())
                .batteryLevel(heartbeat.getBatteryLevel())
                .lat(heartbeat.getLat())
                .lon(heartbeat.getLon())
                .alt(heartbeat.getAlt())
                .build();
    }

    private List<NoSqlHeartbeat> getNoSqlHeartbeats(int fromId, int toId) {
        return MapqlService.getHeartbeats().subList(fromId, toId).stream().map(NosqlMapper::toNoSqlHeartbeat).toList();
    }

}
