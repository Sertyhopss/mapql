package com.example.model.sql;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Flight {

    @JsonProperty("uuid")    // pk
    private String uuid;
    @JsonProperty("drone_uuid")     // fk
    private String droneUuid;
    @JsonProperty("operator_id")    // fk
    private Integer operatorId;
    @JsonProperty("launches")
    private Integer launches;
    @JsonProperty("area")
    private Double area;
    @JsonProperty("route_len")
    private Double routeLen;
    @JsonProperty("survey_object_id")  // fk
    private Integer surveyObjectId;
    @JsonProperty("start_heartbeat_id")
    private Integer startHeartbeatId;
    @JsonProperty("end_heartbeat_id")
    private Integer endHeartbeatId;
}
