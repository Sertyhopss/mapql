package com.example.model.sql;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SurveyObject {
    
    @JsonProperty("id")  // pk
    private int id;
    @JsonProperty("name")
    private String name;
    
}
