package com.example.model.sql;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Heartbeat {

    @JsonProperty("id")            // pk
    private Integer id;
    @JsonProperty("flight_uuid")    // fk
    private String flightUuid;
    @JsonProperty("source")
    private String source;
    @JsonProperty("autopilot_state")
    private Integer autopilotState;
    @JsonProperty("battery_voltage")
    private Double batteryVoltage;
    @JsonProperty("battery_level")
    private Integer batteryLevel;
    @JsonProperty("lat")
    private Double lat;
    @JsonProperty("lon")
    private Double lon;
    @JsonProperty("alt")
    private Double alt;

}
