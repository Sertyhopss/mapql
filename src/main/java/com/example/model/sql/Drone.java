package com.example.model.sql;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Drone {

    @JsonProperty("uuid")     // pk
    private String uuid;
    @JsonProperty("serial")
    private String serial;
    @JsonProperty("imei")
    private String imei;

}
