package com.example.model.sql;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Operator {
    @JsonProperty("id")    // pk
    private Integer id;
    @JsonProperty("login")
    private String login;
    @JsonProperty("name")
    private String name;
    @JsonProperty("company")
    private String company;

}
