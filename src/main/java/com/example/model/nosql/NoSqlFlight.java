package com.example.model.nosql;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@Introspected
public class NoSqlFlight {

    @JsonProperty("flight_uuid")
    private String flightUuid;
    @JsonProperty("drone_uuid")
    private String droneUuid;
    @JsonProperty("drone_serial")
    private String droneSerial;
    @JsonProperty("imei")
    private String imei;
    @JsonProperty("operator_id")
    private Integer operatorId;
    @JsonProperty("operator_login")
    private String operatorLogin;
    @JsonProperty("operator_name")
    private String operatorName;
    @JsonProperty("operator_company")
    private String operatorCompany;
    @JsonProperty("launches")
    private Integer launches;
    @JsonProperty("area")
    private Double area;
    @JsonProperty("route_len")
    private Double routeLen;
    @JsonProperty("survey_object")
    private String surveyObject;
    @JsonProperty("track")
    private List<NoSqlHeartbeat> track;

}
