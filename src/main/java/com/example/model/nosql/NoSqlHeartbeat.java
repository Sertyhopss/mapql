package com.example.model.nosql;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class NoSqlHeartbeat {

    @JsonProperty("flight_uuid")
    private String flightUuid;
    @JsonProperty("source")
    private String source;
    @JsonProperty("autopilot_state")
    private Integer autopilotState;
    @JsonProperty("battery_voltage")
    private Double batteryVoltage;
    @JsonProperty("battery_level")
    private Integer batteryLevel;
    @JsonProperty("timestamp")
    private Instant timestamp;
    @JsonProperty("lat")
    private Double lat;
    @JsonProperty("lon")
    private Double lon;
    @JsonProperty("alt")
    private Double alt;

}
