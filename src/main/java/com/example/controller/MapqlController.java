package com.example.controller;

import com.example.service.MapqlService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class MapqlController {

    @Get("/gen/{size}")
    public HttpResponse<Integer> generate(int size) {
        MapqlService.generateSurveyObjects((int) (size * 0.7));
        MapqlService.generateDrones((int) (size * 0.3));
        MapqlService.generateOperators((int) (size * 0.1));
        MapqlService.generateFlights(size);
        return HttpResponse.ok(MapqlService.getFlights().size());
    }

}
