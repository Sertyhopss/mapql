package com.example.service;

import com.example.model.sql.Drone;
import com.example.model.sql.Flight;
import com.example.model.sql.Heartbeat;
import com.example.model.sql.Operator;
import com.example.model.sql.SurveyObject;

import java.util.List;

public interface IMapqlService {

    List<Operator> getOperators();
    List<Drone> getDrones();
    List<SurveyObject> getSurveyObjects();
    List<Heartbeat> getHeartbeats();
    List<Flight> getFlights();

    List<Operator> generateOperators(int size);

    List<Drone> generateDrones(int size);

    List<SurveyObject> generateSurveyObjects(int size);

     List<Flight> generateFlights(int size) ;

}
