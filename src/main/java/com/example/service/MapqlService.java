package com.example.service;

import com.example.model.sql.Drone;
import com.example.model.sql.Flight;
import com.example.model.sql.Heartbeat;
import com.example.model.sql.Operator;
import com.example.model.sql.SurveyObject;
import com.example.util.Generator;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@UtilityClass
public final class MapqlService {

    private static List<Operator> operators = new ArrayList<>();
    private static Map<String, Drone> drones = new HashMap<>();
    private static List<SurveyObject> surveyObjects = new ArrayList<>();
    private static List<Heartbeat> heartbeats = new ArrayList<>();
    private static Map<String, Flight> flights = new HashMap<>();

    public static List<Operator> generateOperators(int size) {
        List<Operator> res = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            res.add(Generator.generateOperator());
        }
        operators.addAll(res);
        return res;
    }

    public static List<Drone> generateDrones(int size) {
        List<Drone> res = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Drone tmp = Generator.generateDrone();
            res.add(tmp);
            drones.put(tmp.getUuid(), tmp);
        }
        return res;
    }

    public static List<SurveyObject> generateSurveyObjects(int size) {
        List<SurveyObject> res = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            res.add(Generator.generateSurveyObject());
        }
        surveyObjects.addAll(res);
        return res;
    }

    public static List<Flight> generateFlights(int size) {
        List<Flight> res = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Operator operator = operators.get((int) (Math.random() * operators.size()));
            Drone drone = drones.get(drones.keySet().stream().findAny().get());
            SurveyObject surveyObject = surveyObjects.get((int) (Math.random() * surveyObjects.size()));
            Flight flight = Generator.generateFlight(drone, operator, surveyObject);
            res.add(flight);
            flights.put(flight.getUuid(), flight);
        }
        return res;
    }

    public static List<Operator> getOperators() {return MapqlService.operators;}

    public static Map<String, Drone> getDrones() {return MapqlService.drones;}

    public static List<SurveyObject> getSurveyObjects() {return MapqlService.surveyObjects;}

    public static List<Heartbeat> getHeartbeats() {return MapqlService.heartbeats;}

    public static Map<String, Flight> getFlights() {return MapqlService.flights;}
}
