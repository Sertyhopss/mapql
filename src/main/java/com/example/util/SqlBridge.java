package com.example.util;

import io.micronaut.context.annotation.Value;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Singleton
@Slf4j
public class SqlBridge {

    @Value("${postgresql.login}")
    private String login;
    @Value("${postgresql.password}")
    private String password;
    @Value("${postgresql.url}")
    private String url;

    public Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, login, password);
            log.info("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            log.error("Can't connect to PostgreSQL: {}", e.getMessage());
        }

        return conn;
    }

//    public void createDatabase() {
//        try (Connection conn = connect()) {
//            conn.prepareStatement("create database flights " +
//                    "go " +
//                    "")
//        } catch (SQLException e) {
//            log.error("Can't create database: {}", e.getMessage());
//        }
//    }


}
