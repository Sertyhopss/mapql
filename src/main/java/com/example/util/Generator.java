package com.example.util;

import com.example.model.sql.Drone;
import com.example.model.sql.Flight;
import com.example.model.sql.Heartbeat;
import com.example.model.sql.Operator;
import com.example.model.sql.SurveyObject;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class Generator {

    private static final List<String> sources = List.of("source1", "source2", "source3");
    private static int heartbeatId = 0;
    private static int operatorId = 0;
    private static int surveyObjectId = 0;

    private static int heartbeatId() {
        return heartbeatId++;
    }

    private static int operatorId() {
        return operatorId++;
    }

    private static int surveyObjectId() {
        return surveyObjectId++;
    }

    public String generateUUID() {
        return java.util.UUID.randomUUID().toString();
    }

    public int generateInt(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }

    public double generateDouble(double min, double max) {
        return (Math.random() * (max - min) + min);
    }

    public SurveyObject generateSurveyObject() {
        return SurveyObject.builder()
                .id(surveyObjectId())
                .name(generateUUID())
                .build();
    }

    public Drone generateDrone() {
        return Drone.builder()
                .uuid(generateUUID())
                .serial(generateUUID())
                .imei(generateUUID())
                .build();
    }

    public Operator generateOperator() {
        return Operator.builder()
                .id(operatorId())
                .login(generateUUID())
                .name(generateUUID())
                .company(generateUUID())
                .build();
    }

    public Heartbeat generateHeartbeat(String flightUuid) {
        return Heartbeat.builder()
                .id(heartbeatId())
                .flightUuid(flightUuid)
                .source(sources.get(generateInt(0, sources.size() - 1)))
                .autopilotState(generateInt(0, 20))
                .batteryVoltage(generateDouble(0, 100))
                .batteryLevel(generateInt(0, 100))
                .lat(generateDouble(-90, 90))
                .lon(generateDouble(-180, 180))
                .alt(generateDouble(0, 1000))
                .build();
    }

    public Flight generateFlight(Drone drone, Operator operator, SurveyObject surveyObject) {
        Flight flight = Flight.builder()
                .uuid(generateUUID())
                .droneUuid(drone.getUuid())
                .operatorId(operator.getId())
                .launches(generateInt(0, 1000))
                .area(generateDouble(0, 1000))
                .routeLen(generateDouble(0, 1000))
                .surveyObjectId(surveyObject.getId())
                .build();

        List<Heartbeat> heartbeats = generateHeartbeats(flight.getUuid());
        flight.setStartHeartbeatId(heartbeats.get(0).getId());
        flight.setEndHeartbeatId(heartbeats.get(heartbeats.size() - 1).getId());

        return flight;
    }

    private List<Heartbeat> generateHeartbeats(String flightUuid) {
        List<Heartbeat> heartbeats = new ArrayList<>();
        int numHeartbeats = generateInt(0, 100);

        for (int i = 0; i < numHeartbeats; i++) {
            heartbeats.add(generateHeartbeat(flightUuid));
        }

        return heartbeats;
    }
}
